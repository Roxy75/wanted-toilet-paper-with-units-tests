import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import L, { latLng } from "leaflet";
import {
	MapContainer,
	TileLayer,
	useMap,
	Marker,
	Popup,
	useMapEvents,
} from "react-leaflet";
import PinkRollPaper from "../../assets/img/toilet_paper_roll.svg.png";
import BlueRollPaper from "../../assets/img/blue_paper_roll.png";
import BlueCircle from "../../assets/img/bluecircle.png";
import "leaflet/dist/leaflet.css";
import "./home.css";

const Home = () => {
	const [showDetails, setShowDetails] = useState(true);
	const [showMap, setShowMap] = useState(true);
	const [zoom, setZoom] = useState(15);
	const [center, setCenter] = useState([48.818202, 2.285207]);
	const [myPosition, setMyPosition] = useState([48.818202, 2.285207]);
	const [markers, setMarkers] = useState([]);
	const [radius, setRadius] = useState(1500000);

	const toggleDetails = () => {
		setShowDetails(!showDetails);
	};

	const toggleMap = () => {
		setShowMap(!showMap);
	};

	// const iconPinkPaper = new L.Icon({
	// 	iconUrl: PinkRollPaper,
	// 	iconSize: [32, 32],
	// 	iconAnchor: [16, 32],
	// 	popupAnchor: [0, -32],
	// });

	// const iconBluePaper = new L.Icon({
	// 	iconUrl: BlueRollPaper,
	// 	iconSize: [32, 32],
	// 	iconAnchor: [16, 32],
	// 	popupAnchor: [0, -32],
	// });

	const iconHome = new L.Icon({
		iconUrl: BlueCircle,
		iconSize: [32, 32],
		iconAnchor: [16, 32],
		popupAnchor: [0, -32],
	});

	const getMarkerIcon = (id) => {
		switch (id) {
			case 1:
				return L.icon({
					iconUrl: PinkRollPaper,
					iconSize: [50, 50],
					iconAnchor: [25, 50],
					popupAnchor: [0, -50],
				});
			case 2:
				return L.icon({
					iconUrl: BlueRollPaper,
					iconSize: [50, 50],
					iconAnchor: [25, 50],
					popupAnchor: [0, -50],
				});
			default:
				// Return a default icon if the type is not recognized
				return L.icon({
					iconUrl: BlueCircle,
					iconSize: [32, 32],
					iconAnchor: [16, 32],
					popupAnchor: [0, -32],
				});
		}
	};

	useEffect(() => {
		// geoloc
		const fetchData = async () => {
			try {
				const position = await getCurrentPosition();
				setCenter([position.coords.latitude, position.coords.longitude]);
				setMyPosition([position.coords.latitude, position.coords.longitude]);
				getAllServices(position.coords.latitude, position.coords.longitude);
			} catch (error) {
				console.error("Error fetching data:", error);
			}
		};

		fetchData();
		// if (navigator.geolocation) {
		// 	navigator.geolocation.getCurrentPosition((position) => {
		// 		console.log(position.coords);
		// 		// let lat = position.coords.latitude;
		// 		// let lng = position.coords.longitude;

		// 		// this.center = latLng(lat, lng);
		// 		// this.myPosition = latLng(lat, lng);
		// 		// getAllServices(lat,lng);
		// 	});
		// }
	}, []);

	const getCurrentPosition = () => {
		return new Promise((resolve, reject) => {
			navigator.geolocation.getCurrentPosition(resolve, reject);
		});
	};

	const getAllServices = async (lat, lng) => {
		try {
			const deg = radius * 0.0000009;
			const latMin = lat - deg;
			const latMax = lat + deg;
			const longMax = lng + deg / Math.cos((lat * Math.PI) / 180);
			const longMin = lng - deg / Math.cos((lat * Math.PI) / 180);

			fetch(
				`http://localhost:8000/api/search/${latMin}/${latMax}/${longMin}/${longMax}`,
			)
				.then((response) => response.json())
				.then((response) => {
					// console.log(response);
					const results = response.result;
					// console.log(results);
					const newMarkers = results.map((result) => ({
						position: [result.lat, result.lng],
						// name: result.title,
						id: result.id,
						description: result.description,
						type: result.type,
						service_id: result.service_id,
						// phone: result.phone,
						// email: result.email,
						// address: result.address,
						// zip: result.zip,
						// city: result.city,
						firstname: result.firstname,
						name: result.name,
					}));

					setMarkers(newMarkers);
				});
		} catch (error) {
			console.error("Error fetching services:", error);
		}
	};

	const zoomUpdate = (newZoom) => {
		setZoom(newZoom);
	};

	const centerUpdate = (newCenter) => {
		setCenter(newCenter);
	};

	return (
		<main className="home">
			<div style={{ height: "500px", width: "100%" }}>
				<div style={{ height: "200px", overflow: "auto" }}>
					{showDetails && (
						<>
							<p>
								Toi aussi tu as fait les courses et tu es en panne de papier
								toilette ?
							</p>
							<p>Trouve qui peut te dépanner autour de chez toi.</p>
						</>
					)}

					<button onClick={toggleDetails}>Montrer / Cacher les détails</button>

					<button onClick={toggleMap}>Montrer / Cacher la carte</button>
				</div>

				{showMap && (
					<MapContainer
						style={{ height: "300px" }}
						center={center}
						zoom={zoom}
						scrollWheelZoom={false}
						onViewportChanged={(viewport) => {
							zoomUpdate(viewport.zoom);
							centerUpdate([viewport.center.lat, viewport.center.lng]);
						}}
					>
						<TileLayer
							attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
							url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
						/>
						{markers.map((mark) => (
							<Marker
								position={mark.position}
								key={mark.id}
								icon={getMarkerIcon(mark.service_id)}
							>
								{/* {console.log(mark.service_id)} */}
								<Popup>
									<div>
										<h3>
											{mark.name} {mark.firstname}
										</h3>
										<p>
											<strong>Service:</strong> {mark.type}
										</p>
										<p>
											<strong>Description:</strong>{" "}
											{mark.description.substring(0, 30)}...
										</p>

										<Link to={`/details/announce/${mark.id}`}>
											Voir plus de détails
										</Link>
									</div>
								</Popup>
							</Marker>
						))}

						<Marker position={myPosition} icon={iconHome}>
							<Popup>
								<div>Maison</div>
							</Popup>
						</Marker>
					</MapContainer>
				)}
			</div>
			<section className="legend">
				<p>
					<strong>Légende:</strong>
				</p>
				<article className="pinkroll">
					<img src={PinkRollPaper} alt="" />: Don de rouleaux
				</article>
				<article className="blueroll">
					<img src={BlueRollPaper} alt="" />: Troc contre un rouleau
				</article>
			</section>
		</main>
	);
};

export default Home;
