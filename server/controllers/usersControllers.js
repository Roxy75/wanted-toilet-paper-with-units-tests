import pool from "../config/db.js";
import dotenv from "dotenv";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

dotenv.config();
const saltRounds = 10;

export const register = (req, res) => {
	// console.log("req.body", req.body);
	// console.log(req.body.email);

	pool.query(
		"SELECT * FROM users WHERE email = ?",
		[req.body.email],
		(err, user, next) => {
			if (user.length === 0) {
				//équivalent à hashpassword
				bcrypt.hash(req.body.password, saltRounds).then((hash) => {
					pool.query(
						"INSERT INTO users (title, name, firstname, email, password, creationTimestamp) VALUES (?, ?, ?, ?, ?, NOW())",
						[
							req.body.title,
							req.body.name,
							req.body.firstname,
							req.body.email,
							hash,
						],

						(err, result) => {
							//console.log('resultatttt',result.insertId);
							if (err !== null) {
								res.json({
									msg: "Problème lors de la création du compte",
								});
							}

							// sendMail(
							// 	req.body.email,
							// 	'Validation de votre compte',
							// 	'Bienvenu(e) parmi nous !',
							// 	'Te voici à présent membre de la communauté Balance ton voisin. Plus qu’une dernière étape avant de pouvoir accéder à ton profil personnalisé : confirmer ton adresse mail. Pour cela, il te suffit de <a href="http://localhost:5000/validate/user/'+secure_id+'">cliquer sur ce lien.</a>'
							// );
							// console.log(user);

							res.json({
								status: 200,
								okay: true,
								msg: "Enregistré avec succès !",
							});
						},
					);
				});
			} else {
				res.json({
					status: 401,
					msg: "Email déjà utilisé",
				});
			}
		},
	);
};

export const login = (req, res) => {
	// console.log("req.body", req.body);

	pool.query(
		"SELECT * FROM users WHERE email = ?",
		[req.body.email],
		(err, user, next) => {
			// console.log("user", user);

			if (user.length === 0) {
				res.json({
					status: 401,
					msg: "Email non reconnu",
				});
			} else {
				//équivalent à vérifypassword
				bcrypt.compare(req.body.password, user[0].password).then((same) => {
					// console.log("same", same);
					if (same === true) {
						const payload = {
							id: user[0].id,
							name: user[0].name,
							firstname: user[0].firstname,
							email: req.body.email,
						};

						const token = jwt.sign(payload, process.env.JWT_SECRET, {
							expiresIn: process.env.TOKEN_EXPIRE,
						});

						// console.log(token);

						res.json({
							status: 200,
							token: token,
							okay: true,
							user_id: user[0].id,
							msg: "Connexion réussie !",
						});
					} else {
						res.json({
							status: 401,
							msg: "Votre mot de passe est incorrect",
						});
					}
				});
			}
		},
	);
};

export const logout = (req, res) => {
	res.status(200).json({ message: "Déconnexion réussie", okay: true });
	console.log("Session détruite");
};

export const getUser = (req, res) => {
	pool.query("SELECT * FROM users", function (error, user, fields) {
		// console.log(user);
		res.json(user);
	});
};

export const getUserById = (req, res) => {
	let id = req.params.id;
	// console.log(id);
	pool.query(
		"SELECT * FROM users WHERE id = ?",
		[id],
		function (error, user, fields) {
			res.json(user[0]);
		},
	);
};

export const checkToken = async (req, res, next) => {
	//console.log('cool')

	const token =
		req.body.token ||
		req.query.token ||
		req.headers["x-access-token"] ||
		req.cookies.token;

	//on oubli pas de décoder le token pour récup ses infos
	let decode = await jwt.verify(token, process.env.JWT_SECRET);

	console.log("dec", decode);

	pool.query(
		"SELECT * FROM users WHERE email = 'lalande@gmail.com'",
		[decode.email],
		(err, user, next) => {
			if (user === 0) {
				res.json({
					status: 404,
					user: "Pas d'utilisateur",
				});
			} else {
				res.json({
					status: 200,
					user: user,
				});
			}
		},
	);
};
