import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import Home from "./pages/home/Home";
import Register from "./pages/register/Register";
import Login from "./pages/login/Login";
import Board from "./pages/board/Board";
import Form from "./pages/form/Form";
import Announce from "./pages/announce/Announce";
import "./App.css";

const App = ({ isConnected }) => {
	//const isConnected = localStorage.getItem("token");

	//console.log(isConnected);

	return (
		<>
			<BrowserRouter>
				<Header isConnected={isConnected} />
				<Routes>
					{isConnected && [
						<>
							<Route
								path="/dashboard"
								element={<Board isConnected={isConnected} />}
							/>
							<Route
								path="/dashboard/form"
								element={<Form isConnected={isConnected} />}
							/>
							<Route
								path="/details/announce/:id"
								element={<Announce isConnected={isConnected} />}
							/>
						</>,
					]}

					<Route path="/" element={<Home />} />
					<Route path="/register" element={<Register />} />
					<Route path="/login" element={<Login />} />

					<Route path="*" element={<Navigate to="/" />} />
				</Routes>
				<Footer />
			</BrowserRouter>
		</>
	);
};

export default App;
