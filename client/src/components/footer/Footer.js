import React from "react";
import { FaXTwitter, FaInstagram, FaFacebook } from "react-icons/fa6";
import "./footer.css";

const Footer = () => {
	return (
		<footer className="footer">
			<hr className="footer-hr" />
			<nav className="footer-content">
				<ul className="social-icons">
					<li>
						<a href="#" target="_blank" rel="noopener noreferrer">
							<FaXTwitter size={24} />
						</a>
					</li>

					<li>
						<a href="#" target="_blank" rel="noopener noreferrer">
							<FaFacebook size={24} />
						</a>
					</li>
					<li>
						<a href="#" target="_blank" rel="noopener noreferrer">
							<FaInstagram size={24} />
						</a>
					</li>
				</ul>
				<div className="copyright">
					&copy; 2020-2023 Johane. Tous droits réservés.
				</div>
			</nav>
		</footer>
	);
};

export default Footer;
