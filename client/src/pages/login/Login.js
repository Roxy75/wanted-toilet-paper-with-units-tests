import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { IoReturnDownBack } from "react-icons/io5";

import "./login.css";

const Login = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [message, setMessage] = useState("");
	const [show, setShow] = useState(false);
	const [status, setStatus] = useState(false);

	//const [isLogged, setLogged] = useState(false);

	const navigate = useNavigate();

	// const token = localStorage.getItem("token");
	// // const stringToken = JSON.stringify(token);
	// console.log(`Token is ${token}`);

	const handleData = (e) => {
		switch (e.target.name) {
			case "email":
				setEmail(e.target.value);
				// setMessage("");
				break;
			case "password":
				setPassword(e.target.value);
				// setMessage("");
				break;
			case "message":
				setMessage(e.target.value);
				break;
			default:
		}
	};

	const login = async (e) => {
		e.preventDefault();

		if (email.trim() === "" && password.trim() === "") {
			setMessage("Ne laissez pas de champs vides !");
		}

		let datas = {
			email: email,
			password: password,
		};

		try {
			fetch("http://localhost:8000/api/login", {
				method: "POST",
				body: JSON.stringify(datas),
				headers: {
					"Content-Type": "application/json",
				},
			})
				.then((response) => response.json())
				.then((response) => {
					if (response.status === 200) {
						setEmail("");
						setPassword("");
						setMessage(response.msg);
						setStatus(response.okay);
						// console.log(response.msg);
						//setLogged(response.isLogged);
						window.localStorage.setItem("token", response.token);
						//console.log(response.token);
						//setToken(response.token);

						setShow(true);

						setTimeout(() => {
							setShow(false);
							navigate("/dashboard");
						}, 2000);
					} else {
						// console.log(res.data.message);
						setEmail("");
						setPassword("");

						setShow(true);

						setMessage(response.msg);
						setTimeout(() => {
							setShow(false);
						}, 2000);
					}
				});
		} catch (error) {
			if (error) throw error;
		}
	};

	return (
		<main id="login">
			<div id="back">
				<a href="/">
					<IoReturnDownBack /> Accueil
				</a>
			</div>

			<h2>Connexion</h2>

			<form action="/api/login" onSubmit={login}>
				<div className="form_row">
					<label htmlFor="email">Email</label>
					<input
						type="email"
						name="email"
						id="email"
						value={email}
						placeholder="exemple@domaine.com"
						onChange={handleData}
					/>
				</div>

				<div className="form_row">
					<label htmlFor="password1">Mot de passe</label>
					<input
						type="password"
						name="password"
						id="password1"
						value={password}
						placeholder="Mot de passe"
						onChange={handleData}
					/>
				</div>

				{message && show && (
					<p style={{ color: status ? "green" : "red" }}>{message}</p>
				)}

				<div className="form_row">
					<button className="connexion">Connexion</button>
				</div>
			</form>

			<div className="bas">
				<h3>Vous ne possédez pas encore de compte?</h3>
				<a href="/register">
					<button className="create">Créer un compte</button>
				</a>
			</div>
		</main>
	);
};

export default Login;
