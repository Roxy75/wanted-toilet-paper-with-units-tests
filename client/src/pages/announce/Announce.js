import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
//import { jwtDecode } from "jwt-decode";
import { IoReturnDownBack } from "react-icons/io5";

import "./announce.css";

const Announce = ({ isConnected }) => {
	const [details, setDetails] = useState({});

	const { id } = useParams();

	useEffect(() => {
		fetch(`http://localhost:8000/api/details/announce/${id}`, {
			method: "GET",
			headers: {
				// Authorization: `Bearer ${token}`,
				"x-access-token": isConnected,
			},
		})
			.then((response) => response.json())
			.then((response) => {
				// console.log(response.announces);

				if (response.status === 200) {
					setDetails(response.announces);
					console.log(details);
				}
			});
	}, [details, id, isConnected]);

	return (
		<section className="announce">
			<div id="back">
				<Link to="/">
					<IoReturnDownBack /> Accueil
				</Link>
			</div>

			<h2>Détails de l'annonce</h2>

			{/* {details.map((det) => ( */}
			<section className="details">
				<article>
					<p>
						<strong>Service: </strong>
						{details.type}
					</p>
					<p>
						<strong>Description: </strong>
						{details.description}
					</p>
					<p>
						<strong>Nom: </strong>
						{details.name}
					</p>
					<p>
						<strong>Prénom: </strong>
						{details.firstname}
					</p>
					<p>
						<strong>Téléphone: </strong>
						{details.phone}
					</p>
					<p>
						<strong>Email: </strong>
						{details.email}
					</p>
					<p>
						<strong>Adresse: </strong>
						{details.address}
					</p>
					<p>
						<strong>Code Postal: </strong>
						{details.zipcode}
					</p>
					<p>
						<strong>Ville: </strong>
						{details.city}
					</p>
				</article>
			</section>
			{/* ))} */}
		</section>
	);
};

export default Announce;
