import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import { IoReturnDownBack } from "react-icons/io5";

import "./form.css";

const Form = ({ isConnected }) => {
	const [name, setName] = useState("");
	const [firstname, setFirstname] = useState("");
	const [email, setEmail] = useState("");
	const [phone, setPhone] = useState("");
	const [services, setServices] = useState([]);
	const [serviceId, setServiceId] = useState("");
	const [description, setDescription] = useState("");
	const [address, setAddress] = useState("");
	const [zipcode, setZipCode] = useState("");
	const [city, setCity] = useState("");
	const [lat, setLat] = useState(45.44);
	const [lng, setLng] = useState(-6.36);
	const [message, setMessage] = useState("");
	const [show, setShow] = useState(false);
	const [status, setStatus] = useState(false);
	//un state pour gérer l'état du formulaire (disabled ou non)
	//const [disabled, setDisabled] = useState(true);

	const navigate = useNavigate();

	//const token = localStorage.getItem("token");

	//console.log(`token: ${token}`);

	//const { id } = useParams();

	//const cookies = Cookies.get("token");
	// console.log(cookies);

	const decode_token = jwtDecode(isConnected);
	// const decode_token = jwt_decode(token);
	// console.log(`token décodé: ${decode_token}`);

	const { id } = decode_token;
	// console.log(id);

	useEffect(() => {
		fetch(
			"https://nominatim.openstreetmap.org/search.php?q=7+rue+Crespin+du+Gast+75011+Paris&format=jsonv2",
		)
			.then((response) => response.json())
			.then((response) => {
				//console.log(response);
				let lat = response[0].lat;
				let lng = response[0].lon;
				//console.log(lat);
				//console.log(lng);
				//console.log(response.length);
			});

		fetch(`http://localhost:8000/api/user/${id}`, {
			method: "GET",
			headers: {
				"x-access-token": isConnected,
			},
		})
			.then((response) => response.json())
			.then((response) => {
				setName(response.name);
				setFirstname(response.firstname);
				setEmail(response.email);
			});

		fetch(`http://localhost:8000/api/services`, {
			method: "GET",
			headers: {
				"x-access-token": isConnected,
			},
		})
			.then((response) => response.json())
			.then((res) => {
				// console.log(res);
				setServices(res);
			});
	}, [id, isConnected]);

	const handleData = (e) => {
		switch (e.target.name) {
			case "service":
				setServiceId(e.target.value);
				// setMsgError("");
				break;
			case "description":
				setDescription(e.target.value);
				// setMsgError("");
				break;
			case "phone":
				setPhone(e.target.value);
				// setMsgError("");
				break;
			case "address":
				setAddress(e.target.value);
				// setMsgError("");
				break;
			case "zipcode":
				setZipCode(e.target.value);
				// setMsgError("");
				break;
			case "city":
				setCity(e.target.value);
				// setMsgError("");
				break;
			default:
		}
	};

	const submit = (e) => {
		e.preventDefault();

		if (
			!serviceId ||
			description.trim() === "" ||
			phone.trim() === "" ||
			address.trim() === "" ||
			zipcode.trim() === "" ||
			city.trim() === ""
		) {
			setMessage("Ne laissez pas de champs vides !");
		} else {
			// let finalAddress = `${address}``${zipCode}``${city}`;
			// console.log(finalAddress);
			let finalAddress = address + " " + zipcode + " " + city;
			console.log(finalAddress);
			fetch(
				`https://nominatim.openstreetmap.org/search.php?q=${finalAddress}&format=jsonv2`,
			)
				.then((response) => response.json())
				.then((response) => {
					console.log(response);
					if (response.length === 0) {
						console.log("Adresse non trouvée !");
					} else {
						let lat = response[0].lat;
						let lng = response[0].lon;
						console.log(lat);
						console.log(lng);

						let datas = {
							serviceId: serviceId,
							description: description,
							phone: phone,
							address: address,
							zipcode: zipcode,
							city: city,
							lat: lat,
							lng: lng,
							id: id,
						};

						// console.log(datas);

						try {
							fetch("http://localhost:8000/api/add_announces", {
								method: "POST",
								body: JSON.stringify(datas),
								headers: {
									"Content-Type": "application/json",
									"x-access-token": isConnected,
								},
							})
								.then((response) => response.json())
								.then((response) => {
									if (response.status === 200) {
										setServiceId("");
										setDescription("");
										setPhone("");
										setAddress("");
										setZipCode("");
										setCity("");
										setMessage(response.msg);
										console.log(response.msg);

										setStatus(response.okay);
										console.log(response.msg);

										setShow(true);

										setTimeout(() => {
											setShow(false);
											navigate("/dashboard");
										}, 2000);
									} else {
										setServiceId("");
										setDescription("");
										setPhone("");
										setAddress("");
										setZipCode("");
										setCity("");
										setMessage(response.msg);
										console.log(response.msg);

										setShow(true);

										setTimeout(() => {
											setShow(false);
										}, 2000);
									}
								});
						} catch (error) {
							if (error) throw error;
						}
					}
				});
		}
	};

	return (
		<div className="form">
			<div id="back">
				<a href="/">
					<IoReturnDownBack /> Accueil
				</a>
			</div>
			<h2>À la recherche du rouleau manquant</h2>

			<form action="/api/add_announces" onSubmit={submit}>
				<div className="form_row">
					<label>Nom:</label> {name.toUpperCase()}
				</div>

				<div className="form_row">
					<label>Prénom:</label> {firstname}
				</div>

				<div className="form_row">
					<label>Email: </label> {email}
				</div>

				<div className="form_row select">
					<label htmlFor="service">Sélectionner le service</label>
					<select
						name="service"
						id="service"
						value={serviceId}
						onChange={handleData}
					>
						{/* <option defaultValue={true}>Sélectionnez: </option> */}
						<option disabled value="">
							Sélectionnez...
						</option>
						{services.map((serv) => (
							<option value={serv.id} key={serv.id}>
								{serv.type}
							</option>
						))}
					</select>
				</div>

				<div className="form_row">
					<label htmlFor="description">Description</label>
					<textarea
						// cols="80"
						// rows="9"
						name="description"
						id="description"
						value={description}
						placeholder="Description"
						onChange={handleData}
					></textarea>
				</div>

				<div className="form_row">
					<label htmlFor="phone">Numéro de téléphone</label>
					<input
						type="phone"
						name="phone"
						id="phone"
						value={phone}
						placeholder="Ton numéro"
						onChange={handleData}
					/>
				</div>

				<div className="form_row">
					<label htmlFor="address">Adresse</label>
					<input
						type="text"
						name="address"
						id="address"
						value={address}
						placeholder="Ton adresse"
						onChange={handleData}
					/>
				</div>

				<div className="form_row">
					<label htmlFor="zipcode">Code postal</label>
					<input
						type="text"
						name="zipcode"
						id="zipcode"
						value={zipcode}
						placeholder="Ton code postal"
						onChange={handleData}
					/>
				</div>

				<div className="form_row">
					<label htmlFor="city">Ville</label>
					<input
						type="text"
						name="city"
						id="city"
						value={city}
						placeholder="Ta ville"
						onChange={handleData}
					/>
				</div>

				{(message || show) && (
					<p style={{ color: status ? "green" : "red" }}>{message}</p>
				)}

				<button>Envoyer</button>
			</form>
		</div>
	);
};

export default Form;
