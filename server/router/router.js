import express from "express";
import {
	register,
	login,
	logout,
	getUser,
	getUserById,
	checkToken,
} from "../controllers/usersControllers.js";

import {
	getServices,
	addAnnounces,
	getMessagesByUserId,
	getMessageById,
	deleteAnnounce,
	search,
} from "../controllers/announcesControllers.js";

import { withAuth } from "../middlewares/withAuth.js";

const router = express.Router();

/**************** USER *****************/
router.post("/api/register", register);
router.post("/api/login", login);
router.get("/api/logout", logout);
router.get("/api/user", getUser);
router.get("/api/user/:id", getUserById);
router.get("/api/checkToken", withAuth, checkToken);
// router.get("/api/checkToken", checkToken);

/**************** ANOUNCES *****************/
router.get("/api/services", getServices);
router.post("/api/add_announces", addAnnounces);
router.get("/api/announces/:id", withAuth, getMessagesByUserId);
router.get("/api/details/announce/:id", getMessageById);
router.get("/api/delete_announce/:id", deleteAnnounce);
router.get("/api/search/:latmin/:latmax/:lngmin/:lngmax", search);

export default router;
