import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();

//ici on vérifie si le token est là et si il correspond
export const withAuth = (req, res, next) => {
	//console.log("req.headers", req.headers["x-access-token"]);
	const token = req.headers["x-access-token"];

	//console.log("token middleware", token);

	if (token === undefined) {
		res.json({
			status: 404,
			msg: "token not found",
		});
	} else {
		jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
			if (err) {
				res.json({
					status: 401,
					msg: "error, your token is invalid",
				});
			} else {
				next();
			}
		});
	}
};
