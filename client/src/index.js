import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";

const isConnected = localStorage.getItem("token");
// console.log(isConnected);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	<React.StrictMode>
		<App isConnected={isConnected} />
	</React.StrictMode>,
);
