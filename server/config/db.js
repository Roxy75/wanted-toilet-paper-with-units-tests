import mysql from "mysql";
import dotenv from "dotenv";

dotenv.config();

let pool = mysql.createPool({
	connectionLimit: 10000,
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_USER,
	password: process.env.MYSQL_PASSWORD,
	database: process.env.MYSQL_DATABASE,
	socketPath: "/Applications/MAMP/tmp/mysql/mysql.sock",
});

pool.getConnection((err, connection) => {
	console.log("Connected to the database");
	if (err) throw err;
});

export default pool;
