import React, { useState } from "react";
import LogoPaper from "../../assets/img/toiletpaper-logo.png";
import { FaBars, FaTimes } from "react-icons/fa";
import { NavLink, useNavigate } from "react-router-dom";

import "./header.css";

// Revoir le responsive et le menu burger sur la version mobile

const Header = ({ isConnected }) => {
	const [showMenu, setShowMenu] = useState(false);
	const [message, setMessage] = useState("");
	const [show, setShow] = useState(false);
	const [status, setStatus] = useState(false);

	const navigate = useNavigate();

	const toggleMenu = () => {
		setShowMenu(!showMenu);
	};

	const closeMenu = () => {
		setShowMenu(false);
	};

	const logout = () => {
		try {
			fetch("http://localhost:8000/api/logout", {
				method: "GET",
				headers: {
					"x-access-token": isConnected,
				},
			})
				.then((response) => response.json())
				.then((res) => {
					//console.log(res);
					localStorage.removeItem("token");

					setStatus(res.okay);
					setShow(true);

					setTimeout(() => {
						setShow(false);
						navigate("/");
					}, 2000);
				});
		} catch (err) {
			console.log(err);
		}

		closeMenu();
	};

	return (
		<>
			<header>
				<nav className={`menu ${showMenu ? "active" : ""}`}>
					<div>
						<NavLink to="/">
							<img
								className="logo"
								src={LogoPaper}
								alt="rouleau de papier toilettes"
							/>
							PaperRoll Wanted
						</NavLink>
					</div>
					<div className="burger_menu" onClick={toggleMenu}>
						{showMenu ? <FaTimes /> : <FaBars />}
					</div>
					<ul className={showMenu ? "show" : ""}>
						<li>
							<NavLink to="/" onClick={closeMenu}>
								Accueil
							</NavLink>
						</li>

						{isConnected ? (
							<>
								<li>
									<NavLink to="/dashboard" onClick={closeMenu}>
										Mon espace membre
									</NavLink>
								</li>
								<li>
									<NavLink to="/" onClick={logout}>
										Se déconnecter
									</NavLink>
								</li>
							</>
						) : (
							<>
								<li>
									<NavLink to="/register" onClick={closeMenu}>
										S'inscrire
									</NavLink>
								</li>
								<li>
									<NavLink to="/login" onClick={closeMenu}>
										Se connecter
									</NavLink>
								</li>
							</>
						)}
					</ul>
				</nav>
			</header>
			<h1>Trouve ton rouleau de PQ</h1>
		</>
	);
};

export default Header;
