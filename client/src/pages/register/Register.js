import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import "./register.css";

const Register = () => {
	const [title, setTitle] = useState("");
	const [name, setName] = useState("");
	const [firstname, setFirstname] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [message, setMessage] = useState("");
	const [show, setShow] = useState(false);
	const [status, setStatus] = useState(false);

	// const [msgError, setMsgError] = useState("");

	const navigate = useNavigate();

	const handleData = (e) => {
		switch (e.target.name) {
			case "title":
				setTitle(e.target.value);
				// setMsgError("");
				console.log(title);
				break;
			case "name":
				setName(e.target.value);
				// setMsgError("");
				break;
			case "firstname":
				setFirstname(e.target.value);
				// setMsgError("");
				break;
			case "email":
				setEmail(e.target.value);
				// setMsgError("");
				break;
			case "password":
				setPassword(e.target.value);
				// setMsgError("");
				break;
			case "confirmPassword":
				setConfirmPassword(e.target.value);
				// setMsgError("");
				break;
			// case "message":
			// 	setMessage(e.target.value);
			// 	// setMsgError("");
			// 	break;
			default:
		}
	};

	const register = async (e) => {
		if (
			name.trim() === "" ||
			firstname.trim() === "" ||
			email.trim() === "" ||
			password.trim() === "" ||
			confirmPassword.trim() === ""
		) {
			setMessage("Ne laissez pas de champs vides !");
		}

		// ne fonctionne pas correctement. À REVOIR !!
		if (password !== confirmPassword) {
			setMessage("Les mots de passe ne correspondent pas.");
		}

		e.preventDefault();
		let datas = {
			title: title,
			name: name,
			firstname: firstname,
			email: email,
			password: password,
		};

		try {
			fetch("http://localhost:8000/api/register", {
				method: "POST",
				body: JSON.stringify(datas),
				headers: {
					"Content-Type": "application/json",
				},
			})
				.then((response) => response.json())
				.then((response) => {
					if (response.status === 200) {
						setFirstname("");
						setName("");
						setEmail("");
						setPassword("");
						setConfirmPassword("");
						// setMessage("");
						// gérer l'affichage du message de succès avec délai
						setMessage(response.msg);
						setStatus(response.okay);
						// console.log(response.msg);

						setShow(true);

						setTimeout(() => {
							setShow(false);
							navigate("/login");
						}, 2000);
					} else {
						setFirstname("");
						setName("");
						setEmail("");
						setPassword("");
						setConfirmPassword("");
						setMessage(response.msg);
						// console.log(response.msg);

						setShow(true);

						setTimeout(() => {
							setShow(false);
						}, 2000);
					}
				});
		} catch (error) {
			if (error) throw error;
		}
	};

	return (
		<main className="register">
			<h2>Rejoins la communauté</h2>
			<p>Inscris-toi avant qu'il ne soit trop tard !</p>
			<form action="/api/register" onSubmit={register}>
				<div className="form_row title">
					<label htmlFor="title">Titre</label>
					<select name="title" id="title" value={title} onChange={handleData}>
						<option value="Mme">Mme</option>
						<option value="Mlle">Mlle</option>
						<option value="Mr">Mr</option>
					</select>
				</div>
				<div className="form_row">
					<label htmlFor="name">Nom</label>
					<input
						type="text"
						name="name"
						id="name"
						value={name}
						placeholder="Nom"
						onChange={handleData}
					/>
				</div>
				<div className="form_row">
					<label htmlFor="firstname">Prénom</label>
					<input
						type="text"
						name="firstname"
						id="firstname"
						value={firstname}
						placeholder="Prénom"
						onChange={handleData}
					/>
				</div>
				<div className="form_row">
					<label htmlFor="email">Email</label>
					<input
						type="email"
						name="email"
						id="email"
						value={email}
						placeholder="exemple@domaine.com"
						onChange={handleData}
					/>
				</div>
				<div className="form_row">
					<label htmlFor="password1">Mot de passe</label>
					<input
						type="password"
						name="password"
						id="password1"
						placeholder="Mot de passe"
						value={password}
						onChange={handleData}
					/>
				</div>
				<div className="form_row">
					<label htmlFor="password2">Confirmation mot de passe</label>
					<input
						type="password"
						name="confirmPassword"
						id="password2"
						placeholder="Confirmer le mot de passe"
						value={confirmPassword}
						onChange={handleData}
					/>
				</div>

				{message && show && (
					<p style={{ color: status ? "green" : "red" }}>{message}</p>
				)}

				<button className="annonce">Déposer une annonce</button>
			</form>

			<p className="already">Vous êtes déjà inscrit?</p>
			<Link to="/login">
				<button className="btn-already">Se connecter</button>
			</Link>
			{/* <a href="#">Se connecter</a> */}
		</main>
	);
};

export default Register;
