import React, { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import { IoReturnDownBack } from "react-icons/io5";
import { FaTrash } from "react-icons/fa6";

import "./board.css";

const Board = ({ isConnected }) => {
	const [announces, setAnnounces] = useState([]);
	const [message, setMessage] = useState("");
	const [show, setShow] = useState(false);
	const [status, setStatus] = useState(false);

	const navigate = useNavigate();

	useEffect(() => {
		//const token = window.localStorage.getItem("token");
		const decode_token = jwtDecode(isConnected);
		// console.log(decode_token);
		const { id } = decode_token;

		// if (token === null) {
		// 	navigate("/login");
		// } else {
		fetch(`http://localhost:8000/api/announces/${id}`, {
			method: "GET",
			headers: {
				// Authorization: `Bearer ${token}`,
				"x-access-token": isConnected,
			},
		})
			.then((response) => response.json())
			.then((response) => {
				// console.log(response);
				if (response.status === 200) {
					setAnnounces(response.announces);
					// console.log(announces);
				}
			});
		// }
	}, [announces, navigate, isConnected]);

	const deleteAnnounce = (id) => {
		fetch(`http://localhost:8000/api/delete_announce/${id}`, {
			method: "GET",
			headers: {
				"x-access-token": isConnected,
			},
		})
			.then((response) => response.json())
			.then((res) => {
				if (res.status === 200) {
					setAnnounces(announces.filter((anno) => anno.id !== id));
					console.log(res);
					setMessage(res.msg);
					setStatus(res.okay);
					setShow(true);

					setTimeout(() => {
						setShow(false);
					}, 2000);
				} else {
					setMessage(res.msg);
					// console.log(response.msg);
					setShow(true);

					setTimeout(() => {
						setShow(false);
					}, 2000);
				}
			});
	};

	const logout = () => {
		try {
			fetch("http://localhost:8000/api/logout", {
				method: "GET",
				headers: {
					"x-access-token": isConnected,
				},
			})
				.then((response) => response.json())
				.then((res) => {
					//console.log(res);
					localStorage.removeItem("token");

					setStatus(res.okay);
					setShow(true);

					setTimeout(() => {
						setShow(false);
						navigate("/");
					}, 2000);
				});
		} catch (err) {
			console.log(err);
		}
	};

	return (
		<main id="board">
			<div id="back">
				<Link to="/">
					<IoReturnDownBack /> Accueil
				</Link>
			</div>

			<h2>Mon tableau de bord</h2>
			<p>Ici vous pouvez créer votre annonce ou la supprimer</p>
			<div className="div-link-search">
				<Link to="/dashboard/form" className="aide">
					Accéder au formulaire de recherche
				</Link>
			</div>

			{announces.length === 0 ? (
				<section className="bodyFav none">
					<article className="no-announce">
						<p>Aucune annonce à afficher pour l'instant</p>
					</article>
				</section>
			) : (
				<section className="bodyFav">
					{announces.map((ads) => (
						<div key={ads.id} id="announce">
							<div className="spe">
								<p>
									<strong>Service: </strong>
									{ads.type}
								</p>
								<p>
									<strong>Description: </strong>
									{ads.description}
								</p>
							</div>
							<div className="delete">
								<li onClick={() => deleteAnnounce(ads.id)}>
									<FaTrash />
								</li>
							</div>
						</div>
					))}
				</section>
			)}

			{message && show && (
				<p style={{ color: status ? "green" : "red" }}>{message}</p>
			)}

			<div className="div-link-disconnect">
				<Link to="/" className="aide" onClick={logout}>
					Se déconnecter
				</Link>
			</div>
		</main>
	);
};

export default Board;
