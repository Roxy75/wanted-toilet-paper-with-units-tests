import pool from "../config/db.js";
import dotenv from "dotenv";

dotenv.config();
// const saltRounds = 10;

export const getServices = (req, res) => {
	// récupération des catégories depuis la bdd
	pool.query("SELECT * FROM services", function (error, services, fields) {
		// console.log(services);
		res.json(services);
	});
};

export const addAnnounces = (req, res) => {
	// console.log(req.body);
	// Ici faire deux requêtes imbriquées
	// La 1ère pour insérer les infos de la table users
	// puis on récupère l'id du user pour insérer l'annonce dans sa table
	pool.query(
		"INSERT INTO announces (description, address, zipcode, phone, city, lat, lng, user_id, service_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		[
			req.body.description,
			req.body.address,
			req.body.zipcode,
			req.body.phone,
			req.body.city,
			req.body.lat,
			req.body.lng,
			req.body.id,
			req.body.serviceId,
		],
		(err, result) => {
			//console.log('resultatttt',result.insertId);
			// console.log(err);
			if (err !== null) {
				res.json({
					msg: "Échec! Annonce non postée.",
				});
			} else {
				res.json({
					status: 200,
					okay: true,
					msg: "Annonce postée avec succès !",
				});
			}
		},
	);
};

export const getMessagesByUserId = (req, res) => {
	let id = req.params.id;
	pool.query(
		"SELECT announces.id, description, type FROM announces JOIN services WHERE user_id = ? AND service_id = services.id",
		[id],
		(err, result) => {
			//console.log('resultatttt',result.insertId);
			// console.log(result);
			if (err !== null) {
				res.json({
					msg: "Erreur de récupération des messages",
				});
			} else {
				res.json({
					status: 200,
					announces: result,
				});
			}
		},
	);
};

export const deleteAnnounce = (req, res) => {
	let id = req.params.id;

	pool.query("DELETE FROM announces WHERE id = ?", [id], (err, result) => {
		//console.log('resultatttt',result.insertId);
		//console.log(err);
		if (err !== null) {
			res.json({
				msg: "Problème de suppression de l'annonce",
			});
		} else {
			res.json({
				status: 200,
				okay: true,
				msg: "Announce supprimée avec succès !",
			});
		}
	});
};

export const getMessageById = (req, res) => {
	let id = req.params.id;
	pool.query(
		`SELECT announces.id, description, address, zipcode, city, phone, name, firstname, email, type
		FROM announces JOIN users JOIN services
		WHERE announces.id = ? AND service_id = services.id`,
		[id],
		(err, result) => {
			//console.log('resultatttt',result.insertId);
			// console.log(result);
			if (err !== null) {
				res.json({
					msg: "Erreur de récupération des messages",
				});
			} else {
				res.json({
					status: 200,
					announces: result[0],
				});
			}
		},
	);
};

export const search = (req, res) => {
	// console.log(req.params);
	pool.query(
		`SELECT announces.id, description, lat, lng, type, name, firstname, service_id FROM announces JOIN users
		JOIN services WHERE lat > ? AND lat < ? AND lng > ? AND lng < ? AND service_id = services.id AND user_id = users.id`,
		[
			req.params.latmin,
			req.params.latmax,
			req.params.lngmin,
			req.params.lngmax,
		],
		(err, result) => {
			// console.log("resultatttt", result);
			res.json({
				status: 200,
				result: result,
			});
		},
	);
};
