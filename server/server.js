import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import router from "./router/router.js";

const app = express();
const PORT = 8000;

// faire passer les datas dans les cookies
app.use(
	cors({
		origin: ["http://localhost:3000"],
		// origin: "*",
		methods: ["POST, GET"],
		credentials: true,
	}),
);
app.use(express.json());
// app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
// app.use(express.urlencoded({ extended: true }));

app.use(express.static("public"));
// dossier upload photo côté back
// app.use("public/uploads", express.static("uploads"));

app.use("/", router);

app.listen(PORT, () => {
	console.log(`Server started on port http://localhost:${PORT}`);
});
